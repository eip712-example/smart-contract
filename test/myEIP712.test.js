const ethSigUtil = require('eth-sig-util');
const Wallet = require('ethereumjs-wallet').default;

const { domainSeparator } = require('./helpers/eip712');

const EIP712 = artifacts.require('MyEIP712');

contract('MyEIP712', function (accounts) {
  const [bob] = accounts;

  const name = 'DApp that uses claims';
  const version = '1';

  beforeEach('deploying', async function () {
    this.eip712 = await EIP712.new(name, version);

    // We get the chain id from the contract because Ganache (used for coverage) does not return the same chain id
    // from within the EVM as from the JSON RPC interface.
    // See https://github.com/trufflesuite/ganache-core/issues/515
    this.chainId = await this.eip712.getChainId();
  });

  it('domain separator', async function () {
    expect(
      await this.eip712.domainSeparator(),
    ).to.equal(
      await domainSeparator(name, version, this.chainId, this.eip712.address),
    );
  });

  it('digest', async function () {
    const chainId = this.chainId
    const verifyingContract = this.eip712.address
  
    const wallet = Wallet.generate()
    const alice = wallet.getAddressString()

    const Claim = [
      { name: 'nonce', type: 'uint256'},
      { name: 'from', type: 'address'},
      { name: 'to', type: 'address'},
      { name: 'content', type: 'ClaimContent'}
    ]
    
    const ClaimContent = [
      { name: 'text', type: 'string'},
      { name: 'amount', type: 'uint256'},
    ]
  
    const EIP712Domain = [
      { name: 'name', type: 'string' },
      { name: 'version', type: 'string' },
      { name: 'chainId', type: 'uint256' },
      { name: 'verifyingContract', type: 'address' },
    ]
  
    const aliceClaim = {
      nonce: 1,
      from: alice,
      to: bob,
      content: {
        text: 'Alice pays 15 to Bob',
        amount: 15
      }
    }
  
    const domainData = {
      name: name,
      version: version,
      chainId: chainId,
      verifyingContract: verifyingContract
    }
  
    const data = {
      types: {
        EIP712Domain,
        Claim,
        ClaimContent
      },
      domain: domainData,
      primaryType: 'Claim',
      message: aliceClaim,
    }

    const aliceSignature = ethSigUtil.signTypedMessage(wallet.getPrivateKey(), { data });
  

    // TODO use // import { signTypedData, SignTypedDataVersion } from '@metamask/eth-sig-util'
    // const aliceSignature = signTypedData({
    //   data: cryptoSDK._buildTypedClaim(receivedClaim),
    //   privateKey: privateKey1Buffer,
    //   version: SignTypedDataVersion.V4
    // })
    // console.log('aliceSignature', aliceSignature)

    expect(
      await this.eip712.verify(aliceSignature, alice, aliceClaim),
    ).to.equal(true);
  })
})
