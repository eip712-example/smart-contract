// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

// import "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";
// import "@openzeppelin/contracts/utils/cryptography/draft-EIP712.sol";
import "./utils/cryptography/draft-EIP712.sol";
import "./utils/cryptography/ECDSA.sol";

contract MyEIP712 is EIP712 {
    constructor(string memory name, string memory version) EIP712(name, version) {}

    struct ClaimContent {
        string text;
        uint256 amount;
    }

    struct Claim {
        uint256 nonce;
        address from;
        address to;
        ClaimContent content;
    }


    bytes32 private constant CLAIM_CONTENT_TYPE_HASH = keccak256("ClaimContent(string text,uint256 amount)");
    bytes32 private constant CLAIM_TYPE_HASH = keccak256("Claim(uint256 nonce,address from,address to,ClaimContent content)ClaimContent(string text,uint256 amount)");

    function hashClaimContent(ClaimContent calldata claimContent) private pure returns (bytes32) {
        return keccak256(abi.encode(
            CLAIM_CONTENT_TYPE_HASH,
            keccak256(bytes(claimContent.text)),
            claimContent.amount
        ));
    }

    function hashClaim(Claim calldata claim) private pure returns (bytes32) {
        return keccak256(abi.encode(
            CLAIM_TYPE_HASH,
            claim.nonce,
            claim.from,
            claim.to,
            hashClaimContent(claim.content)
        ));
    }

    function verify(
        bytes calldata signature,
        address signer,
        Claim calldata claim
    ) public view returns (bool) {
        bytes32 digest = _hashTypedDataV4(hashClaim(claim));
        address signerRecovered = ECDSA.recover(digest, signature);
        return signerRecovered == signer;
    }

    function getChainId() external view returns (uint256) {
        return block.chainid;
    }

    function domainSeparator() external view returns (bytes32) {
        return _domainSeparatorV4();
    }

}