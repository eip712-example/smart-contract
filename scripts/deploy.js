// const resultDotenv = require('dotenv').config({ path: '.env' })

// if (resultDotenv.error) {
//   throw resultDotenv.error
// }

/**
 * 
 * @param {object} obj should be in the format {variable}
 * @param {string} desc description of the variable
 */
function log(obj, desc="deployed to:") {
    const varToString = varObj => Object.keys(varObj)[0]
    const child = varToString(obj)
    console.log(`${child} ${desc}`, obj[child].address);
}

async function main() {
    console.log("Started!")

    console.log("Importing Hardhat")
    const hre = require("hardhat")
    console.log("Imported.")
    
    console.log("Start to compile")
    await hre.run("compile")
    
    const { artifacts } = hre;
    
    console.log("Importing artifacts")
    const contract = {}
    contract.MyEIP712 = artifacts.require('MyEIP712')
    
    console.log("Artifacts imported.")

    const name = 'DApp that uses claims';
    const version = '1';
    const myEIP712 = await contract.MyEIP712.new(name, version)
    log({myEIP712})
  
  }
  
  main()
    .then(() => process.exit(0))
    .catch((error) => {
      console.error(error);
      process.exit(1);
    });
